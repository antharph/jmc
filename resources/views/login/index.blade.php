@extends('layouts.productdetails')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">{{trans('content.Login')}}</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form id="form-login" class="form-horizontal" role="form" method="POST" action="">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-2 control-label">{{trans('content.Username')}}</label>
							<div class="col-md-10">
								<input type="text" class="form-control" name="username" value="" id="username">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-2 control-label">{{trans('content.Password')}}</label>
							<div class="col-md-10">
								<input type="password" class="form-control" name="password" id="password">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-primary">{{trans('content.Login')}}</button>

								<!-- <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a> -->
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('styles')
<link href="{{url('public/css/navbar.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<script>
$(document).ready(function(){

    var username = "";
    var pass = "";

    $('#form-login').on('submit',function(e){
        e.preventDefault();
        username = $('#username').val();
        pass = $('#password').val();
        $.post( "https://m3.members-support.jp/JapanMap/Api/login_member", { id: username, password: pass })
        .done(function( data ) {
            // console.log(data);
			// var data = JSON.parse(data);
			var result = data.result;
			// console.log(data.expiration_date);
			var expiration = new Date(result.expiration_date);
			expiration.setHours(23);

			var today = new Date();

			if(expiration.getTime() >= today.getTime()){
				login(data);
			}else{
				//expired
				alert("{{trans('content.Expiration_Date_has_expired')}}");
			}
            // login(data);
        })
        .fail(function(){

			$.post( "{{url('login/preauth')}}", { username: username, password: pass, "_token": "{{ csrf_token() }}", })
			.done(function( data ) {
				// console.log(data);
				// var obj = JSON.parse(data);
				var obj = data;
				if(obj.login == 'ok'){
					alert("{{trans('content.Login_successful!')}}");
					window.location.href = '{{url("/")}}'+'/'+obj.redirect;
				}else{
					alert("{{trans('content.Wrong_username_or_password')}}");
				}
			})
            
            $('input').val('');
        });

    });

    function login(data){
        $.post( "{{url('login/auth')}}", { username: username, password: pass, result:data, "_token": "{{ csrf_token() }}", })
        .done(function( data ) {
            // console.log(data);
            // var obj = JSON.parse(data);
            var obj = data;
            if(obj.login == 'ok'){
                alert("{{trans('content.Login_successful!')}}");
                window.location.href = '{{url("/")}}'+'/'+obj.redirect;
            }
        })
    }
    

});
</script>
@endsection
