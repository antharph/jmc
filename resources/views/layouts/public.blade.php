@include('layouts._header')

    <div class="row">
            
            @include('layouts._sidebar')

            @yield('content')

      </div>

@include('layouts._footer')
    
