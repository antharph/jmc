</div> <!-- /container -->

    <div id="top-footer">
        <div class="container">
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9">
              <p>
              地図倶楽部」は、（⼀財）⽇本地図センターが運営する、地図を愛する⼈のための「倶楽部」です。<br/>

「地図倶楽部デジタルライブラリー」では、地図を楽しむ専⾨誌「地図中⼼」電⼦版を毎⽉配信するほか、<br/>
地図・地理に関するさまざまな話題や情報をお届けします。地理の専⾨家の⽅から、「地図を眺めるのが好き！」という⽅まで、幅広くお楽しみいただけるコンテンツを提供してまいります。⼀部コンテンツは「地図倶楽部」会員以外の⽅もご覧いただけます。<br/>
<br/>
≫ <a href="http://www.jmc.or.jp/map-club-opening.html" target="_blank">はじめての⽅へ</a> <br/>
≫ <a href="http://www.jmc.or.jp/map-club/243882.pdf" target="_blank">「地図倶楽部」発⾜に寄せて 〜会員からのメッセージ</a><br/>
≫ <a href="http://www.jmc.or.jp/toiawase.html" target="_blank">お問い合わせ</a><br/>
              </p>
              
            </div>
          </div>
        </div>
    </div>
    <div id="bottom-footer">
      <div class="container">
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9">
              <p>
              地図倶楽部事務局<br/>
〒153-8522　東京都目黒区青葉台4-9-6（一財）日本地図センター　文化事業部内　<br/>
TEL: 03-3485-5417　FAX.: 03-3485-5593<br/>
              </p>
            </div>
          </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{url('public/js/ie10-viewport-bug-workaround.js')}}"></script>

@yield('scripts')

  </body>
</html>