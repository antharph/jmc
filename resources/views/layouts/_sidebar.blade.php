<div class="col-md-3" id="sidebar">
    <form method="get" action="{{url('products/search')}}">
        <input required class="search-box" name="search" type="text" placeholder="{{trans('content.Search')}}" /><span class="glyphicon glyphicon-search search-icon"></span>
        <form>
    <ul>
    <?php
    use App\Category;
    ?>
        @foreach(Category::orderBy('order','asc')->get() as $category)
        <li><a href="{{url('categories/latest/'.$category->id)}}">{{$category->name}}</a></li>
        @endforeach
    </ul>
</div>