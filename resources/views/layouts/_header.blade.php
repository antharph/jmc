
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <meta name="description" content="国土地理院の地図･数値地図･空中写真をはじめとした地図の総合サイト｡全国の地図閲覧､通信販売､地図のQ＆A､地図・GISリンク､GISソフト一覧､電子国土など､多種多様なサービス｡" />
    <meta name="keywords" content="日本地図,数値地図,空中写真,GIS,デジタルマップ,電子国土,財団法人日本地図センター" />
    <title>地図倶楽部｜日本地図センター</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{url('public/bootstrap-3.3.7-dist/css/bootstrap.css')}}">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="{{url('public/css/ie10-viewport-bug-workaround.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->
    @yield('styles')

    <link href="{{url('public/css/styles.css')}}?v={{date('YmdHis')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="top-gray">
      <div class="container">
          <div class="md-col-12">
            <p>〒153-8522 東京都目黒区青葉台4-9-6（一財）日本地図センター 文化事業部内　TEL：03-3485-5417　FAX：03-3485-5593 
            @if(Auth::check())
            <span class="pull-right user-info">ようこそ {{Auth::user()->firstname}} {{Auth::user()->lastname}} さん| 
              @if(Auth::user()->role == 'admin')
              <a href="{{url('admin/products')}}">Admin</a> 
              @endif
            @else
            <!-- <span class="pull-right user-info"> <a href="{{url('login')}}">{{trans('content.Login')}}</a></span> -->
            @endif
            </p>
          </div>
      </div>
    </div>
    <div class="logo-row">
          <div class="container">
            <div class="md-col-12">
              <a id="logo" href="{{url('/')}}" class="pull-left"><img src="{{url('public/images/logo.jpg')}}"/></a>
              @if(Auth::check())
              <a id="violet-btn" href="{{url('login/logout')}}" class="pull-right"><span class="glyphicon glyphicon-star-empty"></span> {{trans('content.Logout')}}</a>
              <a id="bluegreen-btn" href="{{url('members')}}" class="pull-right"><span class="glyphicon glyphicon-send"></span> 会員情報</a></span>
              @else
              <a id="violet-btn" href="{{url('members')}}" class="pull-right"><span class="glyphicon glyphicon-star-empty"></span> 会員ログイン</a>
              <a id="bluegreen-btn" href="https://m3.members-support.jp/JapanMap/Userregistrations/agreement/" class="pull-right"><span class="glyphicon glyphicon-send"></span> 新規会員登録</a>
              @endif
              
            </div>
          </div>
    </div>
    <div class="color-row">
      <div class="half color-1"></div>
      <div class="color-2"></div>
      <div class="color-3"></div>
      <div class="color-4"></div>
      <div class="color-5"></div>
      <div class="color-6"></div>
      <div class="color-7"></div>
      <div class="color-8"></div>
      <div class="half color-9"></div>
    </div>
    <div class="container">
    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <!-- <a class="navbar-brand" href="{{url('/')}}">JMC</a> -->
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class=""><a href="https://jmc.entry-support.com/">地図倶楽部 TOP</a></li>
              <li><a href="http://www.jmc.or.jp/index.html">日本地図センター</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    </div><!-- end container -->

    <div id="menu-shadow"></div>

<div class="container">