      
@extends('layouts.public')

@section('content')
     
            <div class="col-md-9 homepage">
               
                <h2>{{$category->name}}</h2>
                <div class="grid-container ">
                    @foreach($category->products as $product)
                    <div class="grid-item product-block">
                        <a class="" href="{{url('products/details/'.$product->id)}}">
                            <img class="img-responsive" src="{{url($product->image)}}"/>
                            <h3>{{$product->name}}</h3>
                            <!-- <p>{{$product->title1}}</p> -->
                            @if($product->price>=1)
                            <p>{{number_format($product->price,0)}} {{trans('content.yen')}}({{trans('content.tax_included')}})</p>
                            @endif
                            <a class="btn btn-primary view-details" href="{{url('products/details/'.$product->id)}}">{{trans('content.View_Details')}}</a>
                        </a>
                    </div>
                    @endforeach
                </div>
                
            </div>
@endsection

@section('styles')
<link href="{{url('public/css/navbar.css')}}" rel="stylesheet">
@endsection

