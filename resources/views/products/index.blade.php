      
@extends('layouts.public')

@section('content')
     
            <div class="col-md-9 homepage">
                <h2>{{trans('content.News_and_Info')}}</h2>
                <div class="main-list-content">
                    @foreach($newsInfo as $news)
                    <div class="newsInfo">
                        <div class="row">
                        <div class="col-md-2">
                            <p class="news-date">{{\Carbon\Carbon::parse($news->created_at)->format('Y-m-d')}}</p>
                        </div>
                        <div class="col-md-10 pl0">
                            <p><a href="{{$news->url}}" target="_blank">{{$news->title}}</a></p>
                            <!-- <p>{{$news->description}}</p> -->
                            @if($news->description!==null)
                            <!-- <p><a href="{{$news->url}}" target="_blank">{{$news->url}}</a></p> -->
                            @endif
                        </div>
                        </div>
                    </div>
                    @endforeach
                </div>

                <div id="grid-container-4">
    <?php
    use App\Category;
    ?>
        @foreach(Category::orderBy('order','asc')->get() as $category)
        <a class="btn btn-primary" href="{{url('categories/latest/'.$category->id)}}">{{$category->name}}</a>
        @endforeach
                </div>

                <h2 class="digital-lib">{{trans('content.Digital_Library')}}</h2>
                
                <div class="grid-container ">
                @foreach($categories as $category)
                    @foreach($category->products as $product)
                    <div class="grid-item product-block">
                            <img class="img-responsive" src="{{url($product->image)}}"/>
                            <h3>{{$product->name}}</h3>
                            <!-- <p>{{$product->title1}}</p> -->
                            @if($product->price>=1)
                            <p>{{number_format($product->price,0)}} {{trans('content.yen')}}({{trans('content.tax_included')}})</p>
                            @endif
                            <a class="btn btn-primary view-details" href="{{url('products/details/'.$product->id)}}">{{trans('content.View_Details')}}</a>
                    </div>
                    @endforeach
                @endforeach
                </div>
                
            </div>
      
@endsection

@section('styles')
<link href="{{url('public/css/navbar.css')}}" rel="stylesheet">
@endsection

