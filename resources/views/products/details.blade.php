      
@extends('layouts.public')

@section('content')
            <div class="col-md-6">
                    <!-- <img class="img-responsive" src="{{url($product->image)}}"/><br/> -->
                    @if(file_exists($product->image))
                    <img class="product-details-img pull-left" src="{{url($product->image)}}"/>
                    @endif
                    <div class="pull-left product-details-content">
                    <h2>{{$product->name}}</h2>
                        <p class="details-title">{{$product->title1}}</p>
                        <p class="details-title">{{$product->title2}}</p>
                        @if($product->price>=1)
                            <p>{{number_format($product->price,0)}} {{trans('content.yen')}}({{trans('content.tax_included')}})</p>
                            @endif
                        <p>{{$product->description}}</p>
                  @if(file_exists($product->pdf))
                    <a class="btn btn-primary view-pdf" target="_blank" href="{{url('products/viewpdf/'.$product->id)}}">すぐに読む</a>
                    <a class="btn btn-primary download-pdf" href="{{url('products/download/'.$product->id)}}">印刷・PDFダウンロード</a>
                  @endif
                  <a class="btn btn-primary back-to-index" href="{{url('/')}}">{{trans('content.Back_to_index')}}</a>
                  </div>

                  <div class="" style="display: block;clear: both;margin-top: 20px;text-align: center;">
                        <br/>
                  <!-- <a class="btn btn-danger" href="{{url('categories/details/'.$product->category->id)}}">{{trans('content.All_Category_Items')}}</a> -->
                  <a class="btn btn-danger" id="toggle-category-products" href="#">{{trans('content.All_Category_Items')}}</a>
                  </div>

                  <div id="category-products"  class="grid-container " style="display:none;">
                        @foreach($category->products as $product)
                        <div class="grid-item product-block">
                              <a class="" href="{{url('products/details/'.$product->id)}}">
                              <img class="img-responsive" src="{{url($product->image)}}"/>
                              <h3>{{$product->name}}</h3>
                              <!-- <p>{{$product->title1}}</p> -->
                              @if($product->price>=1)
                              <p>{{number_format($product->price,0)}} {{trans('content.yen')}}({{trans('content.tax_included')}})</p>
                              @endif
                              <a class="btn btn-primary view-details" href="{{url('products/details/'.$product->id)}}">{{trans('content.View_Details')}}</a>
                              </a>
                        </div>
                        @endforeach
                  </div>
                  
            </div>
            
      <!-- <iframe class="iframe-pdf" src="{{url('pdfviewer/web/viewer.html?file=')}}/{{$product->pdf}}" /> -->
@endsection

@section('styles')
<link href="{{url('public/css/navbar.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<script>
$(document).ready(function(){

      $('#toggle-category-products').on('click',function(e){
            e.preventDefault();
            $('#category-products').toggle();

      });

});
</script>
@endsection

