@extends('layouts.admin')

@section('content')
<h2 class="sub-header">Edit Banner</h2>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form class="form-horizontal" method="post" action="{{url('admin/banners/edit')}}" enctype="multipart/form-data">
  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">Name (required)</label>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="id" value="{{ $banner->id }}">
    <div class="col-sm-10">
      <input type="text" name="name" class="form-control" id="name" placeholder="Banner Name" value="{{ old('name',$banner->name) }}">
    </div>
  </div>
  <div class="form-group">
    <label for="description" class="col-sm-2 control-label">Description</label>
    <div class="col-sm-10">
      <textarea class="form-control" name="description" rows="3">{{ old('description',$banner->description) }}</textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="image" class="col-sm-2 control-label">Image (required)</label>
    <div class="col-sm-10">
      <input type="file" name="image" class="form-control" id="image" placeholder="Image">
      <a target="_blank" href="{{url($banner->image)}}"><img src="{{url($banner->image)}}"/></a>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary">Update</button>
    </div>
  </div>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){


});
</script>
@endsection