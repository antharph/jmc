<?php use App\Product;?>
@extends('layouts.admin')

@section('content')
<h2 class="sub-header">{{trans('content.Products')}} <a class="btn btn-md btn-primary" href="{{url('admin/products/add')}}">{{trans('content.Add')}}</a> 
<a class="btn btn-md btn-info" href="{{url('admin/products/csv')}}">{{trans('content.CSV_Download')}}</a>
</h2>
<div class="table-responsive">
<table class="table table-striped">
    <thead>
    <tr>
        <th>{{trans('content.Title')}}</th>
        <th>{{trans('content.Category')}}</th>
        <th>{{trans('content.Start_Date')}}</th>
        <th>{{trans('content.End_Date')}}</th>
        <th>{{trans('content.Privacy')}}</th>
        <th>{{trans('content.Action')}}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
    <tr>
        <td>{{$product->name}}</td>
        <td>{{$product->category->name}}</td>
        <td>{{$product->release_date=='0000-00-00'?'':$product->release_date}}</td>
        <td>{{$product->end_date=='0000-00-00'?'':$product->end_date}}</td>
        <td>{{Product::privacy()[$product->privacy]}}</td>
        <td>
        <a class="btn btn-sm btn-primary" href="{{url('admin/products/edit/'.$product->id)}}">{{trans('content.Edit')}}</a> 
        </form>
        
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<?php echo $products->render(); ?>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
</script>
@endsection