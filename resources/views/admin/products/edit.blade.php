<?php use App\Product; ?>
@extends('layouts.admin')

@section('content')
<h2 class="sub-header">{{trans('content.Edit_Product')}}</h2>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form class="form-horizontal" method="post" action="{{url('admin/products/edit')}}" enctype="multipart/form-data">
  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">{{trans('content.Product')}} ({{trans('content.required')}})</label>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="id" value="{{ $product->id }}">
    <div class="col-sm-10">
      <input type="text" name="name" class="form-control" id="name" placeholder="{{trans('content.Product')}}" value="{{ old('name',$product->name) }}">
    </div>
  </div>
  <div class="form-group">
    <label for="title1" class="col-sm-2 control-label">{{trans('content.Title_1')}} </label>
    <div class="col-sm-10">
      <input type="text" name="title1" class="form-control" id="title1" placeholder="" value="{{ old('name',$product->title1) }}">
    </div>
  </div>
  <div class="form-group">
    <label for="title2" class="col-sm-2 control-label">{{trans('content.Title_2')}} </label>
    <div class="col-sm-10">
      <input type="text" name="title2" class="form-control" id="title2" placeholder="" value="{{ old('name',$product->title2) }}">
    </div>
  </div>
  <div class="form-group">
    <label for="description" class="col-sm-2 control-label">{{trans('content.Description')}}</label>
    <div class="col-sm-10">
      <textarea class="form-control" name="description" rows="3">{{ old('description',$product->description) }}</textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="pdf" class="col-sm-2 control-label">PDF ({{trans('content.required')}})</label>
    <div class="col-sm-10">
      <input type="file" name="pdf" class="form-control" id="pdf" placeholder="PDF">
      <a target="_blank" href="{{url($product->pdf)}}">{{trans('content.View')}}</a>
      <p class="help-block">Max 300MB</p>
    </div>
  </div>
  <div class="form-group">
    <label for="image" class="col-sm-2 control-label">{{trans('content.Image')}} ({{trans('content.required')}})</label>
    <div class="col-sm-10">
      <input type="file" name="image" class="form-control" id="image" placeholder="Image">
      <a target="_blank" href="{{url($product->image)}}">{{trans('content.View')}}</a>
    </div>
  </div>
  <div class="form-group">
    <label for="category" class="col-sm-2 control-label">{{trans('content.Category')}} ({{trans('content.required')}})</label>
    <div class="col-sm-9">
      <select class="form-control" name="category_id">
        @foreach($categories as $category)
        <option value="{{$category->id}}" {{ old('category_id',$product->category_id) == $category->id?'selected':'' }}>{{$category->name}}</option>
        @endforeach
      </select>
    </div>
    <div class="col-sm-1">
    <a href="{{url('admin/categories/add')}}" class="btn btn-primary">{{trans('content.Add')}}</a>
    </div>
  </div>
  <div class="form-group">
    <label for="privacy" class="col-sm-2 control-label">{{trans('content.Privacy')}} ({{trans('content.required')}})</label>
    <div class="col-sm-10">
      <select class="form-control" name="privacy">
        @foreach(Product::privacy() as $key => $value)
        <option value="{{$key}}" {{ old('privacy',$product->privacy) == $key?'selected':'' }}>{{$value}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="price" class="col-sm-2 control-label">{{trans('content.Price')}} </label>
    <div class="col-sm-10">
      <input type="text" name="price" class="form-control" id="price" placeholder="" value="{{ old('name',number_format($product->price,0)) }}">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input type="checkbox" name="home_display" value="1" <?php echo $product->home_display=='1'?'checked':'';?>> {{trans('content.Display_on_top_page')}}
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label for="release_date" class="col-sm-2 control-label">{{trans('content.Start_Date')}} </label>
    <div class="col-sm-2">
      <input type="date" name="release_date" class="form-control" id="release_date" placeholder="" value="{{ old('release_date',$product->release_date) }}">
    </div>
  </div>
  <div class="form-group">
    <label for="end_date" class="col-sm-2 control-label">{{trans('content.End_Date')}} </label>
    <div class="col-sm-2">
      <input type="date" name="end_date" class="form-control" id="end_date" placeholder="" value="{{ old('end_date',$product->end_date) }}">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary">{{trans('content.Save')}} </button>
      <a href="#" id="delete-product" class="btn btn-danger">{{trans('content.Delete')}} </a>
    </div>
  </div>
</form>
<form method="post" action="{{url('admin/products/delete')}}" id="delete-form">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="product_id" value="{{$product->id}}"/>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){

  $('#delete-product').on('click',function(e){
    e.preventDefault();

    if(confirm('{{trans('content.Delete')}}')){
      $('#delete-form').submit();
    }

  });

    $('#release_date').datetimepicker({
      format: 'YYYY-MM-DD',
      locale: 'ja'
    });
    $('#end_date').datetimepicker({
        useCurrent: false, //Important! See issue #1075
        format: 'YYYY-MM-DD',
        locale: 'ja'
    });
    $("#release_date").on("dp.change", function (e) {
        $('#end_date').data("DateTimePicker").minDate(e.date);
    });
    $("#end_date").on("dp.change", function (e) {
        $('#release_date').data("DateTimePicker").maxDate(e.date);
    });

    if($('#release_date').val().length > 0){
      $('#end_date').data("DateTimePicker").minDate($('#release_date').val());
    }

    if($('#end_date').val().length > 0){
      $('#release_date').data("DateTimePicker").maxDate($('#end_date').val());
    }

});
</script>
@endsection