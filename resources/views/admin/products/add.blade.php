<?php use App\Product; ?>
@extends('layouts.admin')

@section('content')
<h2 class="sub-header">{{trans('content.Add_Product')}}</h2>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form class="form-horizontal" method="post" action="{{url('admin/products/add')}}" enctype="multipart/form-data">
  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">{{trans('content.Product')}} ({{trans('content.required')}})</label>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-sm-10">
      <input type="text" name="name" class="form-control" id="name" placeholder="{{trans('content.Product')}}" value="{{ old('name') }}">
    </div>
  </div>
  <div class="form-group">
    <label for="title1" class="col-sm-2 control-label">{{trans('content.Title_1')}} </label>
    <div class="col-sm-10">
      <input type="text" name="title1" class="form-control" id="title1" placeholder="" value="{{ old('title1') }}">
    </div>
  </div>
  <div class="form-group">
    <label for="title2" class="col-sm-2 control-label">{{trans('content.Title_2')}} </label>
    <div class="col-sm-10">
      <input type="text" name="title2" class="form-control" id="title2" placeholder="" value="{{ old('title2') }}">
    </div>
  </div>
  <div class="form-group">
    <label for="description" class="col-sm-2 control-label">{{trans('content.Description')}}</label>
    <div class="col-sm-10">
      <textarea class="form-control" name="description" rows="3">{{ old('description') }}</textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="pdf" class="col-sm-2 control-label">PDF ({{trans('content.required')}})</label>
    <div class="col-sm-10">
      <input type="file" name="pdf" class="form-control" id="pdf" placeholder="PDF">
      <p class="help-block">Max 300MB</p>
    </div>
  </div>
  <div class="form-group">
    <label for="image" class="col-sm-2 control-label">{{trans('content.Thumbnail_Image')}} ({{trans('content.required')}})</label>
    <div class="col-sm-10">
      <input type="file" name="image" class="form-control" id="image" placeholder="{{trans('content.Thumbnail_Image')}}">
    </div>
  </div>
  <div class="form-group">
    <label for="category" class="col-sm-2 control-label">{{trans('content.Category')}} ({{trans('content.required')}})</label>
    <div class="col-sm-9">
      <select class="form-control" name="category_id">
        @foreach($categories as $category)
        <option value="{{$category->id}}" {{ old('category_id') == $category->id?'selected':'' }}>{{$category->name}}</option>
        @endforeach
      </select>
    </div>
    <div class="col-sm-1">
    <a href="{{url('admin/categories/add')}}" class="btn btn-primary">{{trans('content.Add')}}</a>
    </div>
  </div>
  <div class="form-group">
    <label for="privacy" class="col-sm-2 control-label">{{trans('content.Privacy')}} ({{trans('content.required')}})</label>
    <div class="col-sm-10">
      <select class="form-control" name="privacy">
        @foreach(Product::privacy() as $key => $value)
        <option value="{{$key}}" {{ old('privacy') == $key?'selected':'' }}>{{$value}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="price" class="col-sm-2 control-label">{{trans('content.Price')}} </label>
    <div class="col-sm-10">
      <input type="text" name="price" class="form-control" id="price" placeholder="" value="{{ old('price') }}">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input type="checkbox" name="home_display" value="1"> {{trans('content.Display_on_top_page')}}
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label for="release_date" class="col-sm-2 control-label">{{trans('content.Start_Date')}} </label>
    <div class="col-sm-2">
      <input type="date" name="release_date" class="form-control" id="release_date" placeholder="" value="{{ old('release_date') }}">
    </div>
  </div>
  <div class="form-group">
    <label for="end_date" class="col-sm-2 control-label">{{trans('content.End_Date')}} </label>
    <div class="col-sm-2">
      <input type="date" name="end_date" class="form-control" id="end_date" placeholder="" value="{{ old('end_date') }}">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary">{{trans('content.Save')}} </button>
    </div>
  </div>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
$(function () {
    $('#release_date').datetimepicker({
      format: 'YYYY-MM-DD',
      locale: 'ja'
    });
    $('#end_date').datetimepicker({
        useCurrent: false, //Important! See issue #1075
        format: 'YYYY-MM-DD',
        locale: 'ja'
    });
    $("#release_date").on("dp.change", function (e) {
        $('#end_date').data("DateTimePicker").minDate(e.date);
    });
    $("#end_date").on("dp.change", function (e) {
        $('#release_date').data("DateTimePicker").maxDate(e.date);
    });
  });

</script>
@endsection