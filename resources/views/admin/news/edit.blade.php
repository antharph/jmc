<?php use App\Product; ?>
@extends('layouts.admin')

@section('content')
<h2 class="sub-header">{{trans('content.Edit_News/Info')}}</h2>
@include('admin._errors')
<form class="form-horizontal" method="post" action="{{url('admin/news/edit')}}" enctype="multipart/form-data">
    <!-- <div class="form-group">
        <label for="slug" class="col-sm-2 control-label">Slug (required)</label>
        <div class="col-sm-10">
        <input type="text" name="slug" class="form-control" id="slug" readonly="readonly" value="{{ $newsInfo->slug }}">
        </div>
    </div> -->
  <div class="form-group">
    <label for="title" class="col-sm-2 control-label">{{trans('content.Title')}} ({{trans('content.required')}})</label>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="news_info_id" class="form-control" value="{{ $newsInfo->id }}">
    <div class="col-sm-10">
      <input type="text" name="title" class="form-control" id="title" placeholder="{{trans('content.Title')}}" value="{{ old('title',$newsInfo->title) }}">
    </div>
  </div>
  <!-- <div class="form-group">
    <label for="description" class="col-sm-2 control-label">{{trans('content.Description')}}</label>
    <div class="col-sm-10">
      <textarea class="form-control" name="description" rows="3">{{ old('description',$newsInfo->description) }}</textarea>
    </div>
  </div> -->
  <div class="form-group">
    <label for="url" class="col-sm-2 control-label">{{trans('content.URL')}}</label>
    <div class="col-sm-10">
      <input type="text" name="url" class="form-control" id="url" placeholder="{{trans('content.URL')}}" value="{{ old('url',$newsInfo->url) }}">
    </div>
  </div>
  <div class="form-group">
    <label for="privacy" class="col-sm-2 control-label">{{trans('content.Display')}} ({{trans('content.required')}})</label>
    <div class="col-sm-10">
      <select class="form-control" name="privacy">
        @foreach(Product::privacy() as $key => $value)
        <option value="{{$key}}" {{ old('privacy',$newsInfo->privacy) == $key?'selected':'' }}>{{$value}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary">{{trans('content.Save')}}</button> 
      <a href="#" id="delete-news" class="btn btn-danger">{{trans('content.Delete')}} </a>
    </div>
  </div>
</form>

<form method="post" action="{{url('admin/news/delete')}}" id="delete-form">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="news_id" value="{{$newsInfo->id}}"/>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){

  $('#delete-news').on('click',function(e){
    e.preventDefault();

    if(confirm('{{trans('content.Delete')}}')){
      $('#delete-form').submit();
    }

  });

});
</script>
@endsection