@extends('layouts.admin')

@section('content')
<h2 class="sub-header">{{trans('content.Access_Analytics')}}</h2>
<!-- <script src="{{url('public/Highcharts-6.1.0/code/highcharts.js')}}"></script>
<script src="{{url('public/Highcharts-6.1.0/code/modules/series-label.js')}}"></script>
<script src="{{url('public/Highcharts-6.1.0/code/modules/exporting.js')}}"></script>
<script src="{{url('public/Highcharts-6.1.0/code/modules/export-data.js')}}"></script> -->
<!-- <div id="container" style="width:100%; height:400px;"></div> -->
<div class="table-responsive">
<table class="table table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>{{trans('content.Category')}}</th>
        <th>{{trans('content.Product')}}</th>
        <th colspan="2" class="text-center">{{trans('content.Today')}}</th>
        <th colspan="2" class="text-center">{{trans('content.This_Week')}}</th>
        <th colspan="2" class="text-center">{{trans('content.This_Month')}}</th>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td class="text-center">{{trans('content.View_Product_Page')}}</td>
        <td class="text-center">{{trans('content.PDF_Download')}}</td>
        <td class="text-center">{{trans('content.View_Product_Page')}}</td>
        <td class="text-center">{{trans('content.PDF_Download')}}</td>
        <td class="text-center">{{trans('content.View_Product_Page')}}</td>
        <td class="text-center">{{trans('content.PDF_Download')}}</td>
    </tr>
    </thead>
    <tbody>
    <?php $count = 1; ?>
    @foreach($products as $product)
    <tr>
        <td>{{$count}}</td>
        <td>{{$product->category_name}}</td>
        <td>{{$product->name}}</td>
        <td class="text-center">{{$product->details_today>0?$product->details_today:''}}</td>
        <td class="text-center">{{$product->download_today>0?$product->download_today:''}}</td>
        <td class="text-center">{{$product->details_this_week>0?$product->details_this_week:''}}</td>
        <td class="text-center">{{$product->download_this_week>0?$product->download_this_week:''}}</td>
        <td class="text-center">{{$product->details_this_month>0?$product->details_this_month:''}}</td>
        <td class="text-center">{{$product->download_this_month>0?$product->download_this_month:''}}</td>
    </tr>
    <?php $count++; ?>
    @endforeach
    </tbody>
</table>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
// Highcharts.chart('container', {

// credits: {
//     enabled: false
// },

// title: {
//     text: 'Title'
// },

// // subtitle: {
// //     text: 'Source: thesolarfoundation.com'
// // },

// yAxis: {
//     title: {
//         text: 'Views'
//     }
// },
// legend: {
//     layout: 'vertical',
//     align: 'right',
//     verticalAlign: 'middle'
// },

// plotOptions: {
//     series: {
//         label: {
//             connectorAllowed: false
//         },
//         pointStart: 2010
//     }
// },

// series: [{
//     name: 'Installation',
//     data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
// }, {
//     name: 'Manufacturing',
//     data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
// }, {
//     name: 'Sales & Distribution',
//     data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
// }, {
//     name: 'Project Development',
//     data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
// }, {
//     name: 'Other',
//     data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
// }],

// responsive: {
//     rules: [{
//         condition: {
//             maxWidth: 500
//         },
//         chartOptions: {
//             legend: {
//                 layout: 'horizontal',
//                 align: 'center',
//                 verticalAlign: 'bottom'
//             }
//         }
//     }]
// }

// });
</script>
@endsection