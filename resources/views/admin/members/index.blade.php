@extends('layouts.public')

@section('content')
            <div class="col-md-6">
                    <h2>会員情報</h2>
                    <p>氏名（姓）: {{$user->lastname}}</p>
                    <p>氏名（名）: {{$user->firstname}}</p>
                    <p>会員種別 : {{$user->membership_type}}</p>
                    <p>会員有効期限 {{date('Y年m月d日',strtotime($user->expiration_date))}}</p>
                    <br/>
                    <p><a class="btn btn-sm btn-primary" href="https://m3.members-support.jp/JapanMap/">会費支払い</a>（カード決済・コンビニ支払いはこちら）</p>
                    <br/>
                    <p>
                    ※ 郵便局から振込支払をご希望の方は事務局までお知らせください。<br/>
                        メール：map-club@jmc.or.jp<br/>
                        電話：03-3485-5417
                        </p>
            </div>
@endsection

@section('styles')
<link href="{{url('public/css/navbar.css')}}" rel="stylesheet">
@endsection

