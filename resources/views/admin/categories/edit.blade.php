@extends('layouts.admin')

@section('content')
<h2 class="sub-header">{{trans('content.Edit_Category')}}</h2>
@include('admin._errors')
<form class="form-horizontal" method="post" action="{{url('admin/categories/edit')}}" enctype="multipart/form-data">
    <!-- <div class="form-group">
        <label for="slug" class="col-sm-2 control-label">Slug</label>
        <div class="col-sm-10">
        <input type="text" readonly="readonly" name="slug" class="form-control" id="slug" value="{{ old('slug',$category->slug) }}">
        </div>
    </div> -->
  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">{{trans('content.Category')}} ({{trans('content.required')}})</label>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="category_id" value="{{ old('id',$category->id) }}">
    <div class="col-sm-10">
      <input type="text" name="name" class="form-control" id="name" placeholder="{{trans('content.Category')}}" value="{{ old('name',$category->name) }}">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary">{{trans('content.Save')}}</button> 
      <a href="#" id="delete-category" class="btn btn-danger">{{trans('content.Delete')}} </a>
    </div>
  </div>
</form>

<form method="post" action="{{url('admin/categories/delete')}}" id="delete-form">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="category_id" value="{{$category->id}}"/>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){

  $('#delete-category').on('click',function(e){
    e.preventDefault();

    if(confirm('{{trans('content.Delete')}}')){
      $('#delete-form').submit();
    }

  });

});
</script>
@endsection