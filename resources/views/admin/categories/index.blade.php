@extends('layouts.admin')

@section('content')
<h2 class="sub-header">{{trans('content.Categories')}} <a class="btn btn-md btn-primary" href="{{url('admin/categories/add')}}">{{trans('content.Add')}}</a> 
<button id="update-order" class="btn btn-info pull-right">{{trans('content.Save')}}</button></h2>
<div class="table-responsive">
<form id="categories-table-form">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<table class="table table-striped admin">
    <thead>
    <tr>
        <th>{{trans('content.Category')}}</th>
        <th>{{trans('content.Date_Update')}}</th>
        <th>{{trans('content.Sort_by')}}</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($categories as $category)
    <tr>
        <td>{{$category->name}}</td>
        <td>{{$category->updated_at}}</td>
        <td data-order="{{$category->order}}" class="sorting-col"><input class="text-center" type="text" name="order[{{$category->id}}]" value="{{$category->order}}" />
        <a href="#" class="up text-danger up-down"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a> 
        <a href="#" class="down text-danger up-down"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
        </td>
        <td>
        <a class="btn btn-sm btn-primary" href="{{url('admin/categories/products/'.$category->id)}}">{{trans('content.View_Category_Products')}} ({{count($category->products)}})</a> 
        <a class="btn btn-sm btn-primary" href="{{url('admin/categories/edit/'.$category->id)}}">{{trans('content.Edit')}}</a> 
        </form>
        
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
</form>

<?php echo $categories->render(); ?>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
    $('#update-order').on('click',function(e){
        e.preventDefault();
        $.post( "{{url('admin/categories/updateorder')}}", $( "#categories-table-form" ).serialize() )
        .done(function($data){
            window.location.reload();
        })
    });

    $('.down').on('click',function(e){
        e.preventDefault()
        var current = $(this).closest('tr');
        var next = current.next('tr');
        swapOrder(current,next);
        current.next().after(current);
    });

    $('.up').on('click',function(e){
        e.preventDefault()
        var current = $(this).closest('tr');
        var prev = current.prev('tr');
        swapOrder(current,prev);
        current.prev().before(current);
    });

    function swapOrder(current,next){
        var currentOrder = current.find('input[name*=order]').val();
        var nextOrder = next.find('input[name*=order]').val();

        current.find('input[name*=order]').val(nextOrder);
        next.find('input[name*=order]').val(currentOrder);
        console.log(currentOrder);
    }
});
</script>
@endsection