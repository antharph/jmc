@extends('layouts.admin')

@section('content')
<h2 class="sub-header">{{trans('content.Add_Category')}}</h2>
@include('admin._errors')
<form class="form-horizontal" method="post" action="{{url('admin/categories/add')}}" enctype="multipart/form-data">
  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">{{trans('content.Category')}} ({{trans('content.required')}})</label>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-sm-10">
      <input type="text" name="name" class="form-control" id="name" placeholder="{{trans('content.Category')}}" value="{{ old('name') }}">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary">{{trans('content.Save')}}</button>
    </div>
  </div>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
</script>
@endsection