<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTitlesToProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('products', function(Blueprint $table)
		{
			$table->string('title1')->nullable()->after('name');
			$table->string('title2')->nullable()->after('title1');
			$table->decimal('price', 5, 2)->nullable()->after('title2');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('products', function(Blueprint $table)
		{
			$table->dropColumn('title1');
			$table->dropColumn('title2');
			$table->dropColumn('price');
		});
	}

}
