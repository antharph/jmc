<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('slug')->nullable();
			$table->string('name')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});

		Schema::create('news_infos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('slug')->nullable();
			$table->string('title')->nullable();
			$table->text('description')->nullable();
			$table->integer('order')->nullable();
			$table->integer('privacy')->nullable()->default(1);
			$table->softDeletes();
			$table->timestamps();
		});

		Schema::table('products', function(Blueprint $table)
		{
			$table->integer('category_id')->after('id')->nullable();
			$table->integer('privacy')->after('pdf')->default(1)->comment('0 - public, 1 - member only');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories');
		Schema::drop('news_infos');

		Schema::table('products', function (Blueprint $table) {
			$table->dropColumn('category_id');
			$table->dropColumn('privacy');
		});
	}

}
