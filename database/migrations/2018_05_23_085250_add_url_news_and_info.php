<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUrlNewsAndInfo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('news_infos', function(Blueprint $table)
		{
			$table->string('url')->after('description')->nullable();
		});

		Schema::table('categories', function(Blueprint $table)
		{
			$table->integer('order')->after('name')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('news_infos', function(Blueprint $table)
		{
			$table->dropColumn('url');
		});

		Schema::table('categories', function(Blueprint $table)
		{
			$table->dropColumn('order');
		});
	}

}
