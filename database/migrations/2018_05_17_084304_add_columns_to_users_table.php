<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->string('role')->nullable();
			$table->string('username')->nullable();
			$table->string('member_number')->nullable();
			$table->string('member_kind_cd')->nullable();
			$table->string('expiration_date')->nullable();
			$table->string('status')->nullable();
			$table->string('classification')->nullable();
			$table->string('lastname')->nullable();
			$table->string('firstname')->nullable();
			$table->string('lastfurigana')->nullable();
			$table->string('firstfurigana')->nullable();
			$table->string('sex')->nullable();
			$table->string('birthday')->nullable();
			$table->string('addr1_prefecture')->nullable();
			$table->string('mail_address')->nullable();
			$table->string('mail_address2')->nullable();
			$table->string('mail_address3')->nullable();
			$table->string('mail_address4')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropColumn('role');
			$table->dropColumn('username');
			$table->dropColumn('member_number');
			$table->dropColumn('member_kind_cd');
			$table->dropColumn('expiration_date');
			$table->dropColumn('status');
			$table->dropColumn('classification');
			$table->dropColumn('lastname');
			$table->dropColumn('firstname');
			$table->dropColumn('lastfurigana');
			$table->dropColumn('firstfurigana');
			$table->dropColumn('sex');
			$table->dropColumn('birthday');
			$table->dropColumn('addr1_prefecture');
			$table->dropColumn('mail_address');
			$table->dropColumn('mail_address2');
			$table->dropColumn('mail_address3');
			$table->dropColumn('mail_address4');
		});
	}

}
