<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Banner;
use App\NewsInfo;
use App\Category;
use Auth;

class ProductsController extends Controller {

    const LIMIT = 10;

	public function __construct()
	{
		// $this->middleware('auth');
    }

    public function getIndex(){
        $categories = Category::with(['products' => function($query)
        {
            $query = Product::publicCondition($query);
            $query->where('home_display', '=',1);
            
        }])
        ->orderBy('order','asc')
        ->get();

        $data['categories'] = $categories;
        // $banner = Banner::first();
        // $data['banner'] = $banner;
        // $data['products'] = Product::orderBy('id','desc')->paginate(self::LIMIT);
        $data['newsInfo'] = NewsInfo::where(function($query){
            $query->where('order','!=',0);
            if (!Auth::check()){
                $query->where('privacy', '=',0);
            }

        })->orderBy('order','asc')->get();
        return view('products.index',$data);
    }

    public function getDetails($id=null){

        // if (!Auth::check()){
        //     return redirect('auth/login');
        // }

        $product = Product::with(['category'])->find($id);
        if($product == null)
            abort('Product not found');

        if($product->privacy == '1' && !Auth::check()){
            return redirect('login');
        }

        $product->analytics()->create(['product_id' => $product->id,'type'=>'dt']);

        $category = Category::with(['products' => function($query) use($product)
        {
            $query = Product::publicCondition($query)->where('id','!=',$product->id);
            $query->orderBy('order_in_category','asc');
            
        }])->find($product->category->id);
        $data['category'] = $category;

        $data['product'] = $product;
        return view('products.details',$data);
    }

    public function getViewpdf($product_id){
        $product = Product::find($product_id);

        if($product == null){
            abort(404);
        }

        // $product->analytics()->create(['product_id' => $product->id]);

        return redirect(url('pdfviewer/web/viewer.html?file=/'.$product->pdf));
    }

    public function getDownload($product_id){

        $product = Product::find($product_id);

        if($product == null){
            abort(404);
        }

        $product->analytics()->create(['product_id' => $product->id,'type'=>'dl']);

        header("Content-disposition: attachment; filename=pdf-".\Carbon\Carbon::now()->timestamp.".pdf");
        header("Content-type: application/pdf");
        readfile("$product->pdf");

    }

    public function getSearch(Request $request){
        $query = Product::where(function($query2) use($request){
            $query2->where('name','like','%'.$request->input('search').'%')
                ->orWhere('title1','like','%'.$request->input('search').'%')
                ->orWhere('title2','like','%'.$request->input('search').'%')
                ->orWhere('description','like','%'.$request->input('search').'%')
                ->orWhere('price','like','%'.$request->input('search').'%');
        });

        if (!Auth::check()){
            $query->where('privacy', '=',0);
        }

        $data['products'] = $query->get();
        return view('products.search',$data);
    }
   
}