<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use Auth;
use App\User;
use Hash;

class LoginController extends Controller {


	public function __construct()
	{
		// $this->middleware('auth');
    }

    public function getIndex(){
        if(Auth::check()){
            return redirect('/');
        }

        return view('login.index');
    }

    public function postPreauth(Request $request){
        $result['login'] = 'failed';
        $result['redirect'] = '';
        $user = User::where('email',$request->username)->first();

        if($user !== null && Hash::check($request->password, $user->password)){
            Auth::loginUsingId($user->id);
            $result['login'] = 'ok';

            if($user->role == 'admin'){
                $result['redirect'] = 'admin/products';
            }
        }

        return $result;

    }

    public function postAuth(Request $request){
        //admin 
        //SopAdmin
        //sknadmin@
        // dd($request->result['result']['last_login']);
        $result = array();
        $result['login'] = 'failed';
        $result['redirect'] = '';
        $apiData = $request->result['result'];
        if( !is_null($apiData['last_login']) ){
            $user = User::where('username',$request->username)->first();
            if($user == null){
                unset($apiData['last_login']);
                unset($apiData['modified']);
                $apiData['email'] = $request->username;
                $apiData['username'] = $request->username;
                $newUser = new User($apiData);
                $newUser->save();
                Auth::loginUsingId($newUser->id);
                $result['user'] = $newUser;

                if($newUser->role == 'admin'){
                    $result['redirect'] = 'admin/products';
                }

            }else{
                Auth::loginUsingId($user->id);
                $result['user'] = $user;

                if($user->role == 'admin'){
                    $result['redirect'] = 'admin/products';
                }
            }
        }

        if(Auth::check()){
            $result['login'] = 'ok';
        }
        
        return $result;
    }

    public function getLogout(){
        Auth::logout();
        return redirect('/');
    }

}