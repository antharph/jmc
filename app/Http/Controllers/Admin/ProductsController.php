<?php 
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Product;
use App\Category;
use Excel;

class ProductsController extends Controller {

	private $pdf_location;
	private $image_location;
	const LIMIT = 10;
	private $product;

	public function __construct()
	{
		// $this->middleware('auth');
		$this->pdf_location = null;
		$this->image_location = null;
	}

	public function getIndex()
	{
		$data['products'] = Product::with(['category'])->orderBy('id','desc')->paginate(self::LIMIT);
		return view('admin.products.products',$data);
	}

	public function getAdd(){
		$categories = Category::all();
		return view('admin.products.add',['categories'=>$categories]);
	}

	public function postAdd(Request $request){
		
		$validator = $this->validateProduct($request);

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator)->withInput();
		}
		
		$product = new Product;
		$product->name = $request->input('name');
		$product->description = $request->input('description');
		$product->pdf = $this->pdf_location;
		$product->image = $this->image_location;
		$product->category_id =  $request->input('category_id');
		$product->privacy =  $request->input('privacy');
		$product->title1 =  $request->input('title1');
		$product->title2 =  $request->input('title2');
		$product->price =  $request->input('price');
		$product->home_display =  $request->input('home_display');

		if(!empty($request->input('release_date')))
			$product->release_date =  $request->input('release_date');

		if(!empty($request->input('end_date')))
			$product->end_date =  $request->input('end_date');

		$product->save();
		
		return redirect('admin/products')->with('success', trans('content.Product_saved!'));
	}

	public function getEdit($id=null){
		$product = Product::find($id);
		if($product == null)
			abort(404);

		$data['product'] = $product;
		$data['categories'] = Category::all();
		return view('admin.products.edit',$data);
	}

	public function postEdit(Request $request){
		$id = $request->input('id');
		$this->product = Product::find($id);
		if($this->product == null)
			abort(404);

		$validator = $this->validateProduct($request);

		$validator->after(function($validator) use ($request)
		{
			$product_name = $request->input('name');
			//product name must be unique
			if($this->product->name !== $product_name){
				$productSearch = Product::where('name',$product_name)->first();
				if($productSearch !== null){
					$validator->errors()->add('name', trans('content.Product_name_already_exist'));
				}
				
			}
		});

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator)->withInput();
		}

		$this->product->name = $request->input('name');
		$this->product->description = $request->input('description');
		$this->product->category_id =  $request->input('category_id');
		$this->product->privacy =  $request->input('privacy');
		$this->product->title1 =  $request->input('title1');
		$this->product->title2 =  $request->input('title2');
		$this->product->price =  $request->input('price');
		$this->product->home_display =  $request->input('home_display');
		
		if(!empty($request->input('release_date')))
			$this->product->release_date =  $request->input('release_date');

		if(!empty($this->product->release_date) && empty($request->input('release_date')))
			$this->product->release_date = null;

		if(!empty($request->input('end_date')))
			$this->product->end_date =  $request->input('end_date');

		if(!empty($this->product->end_date) && empty($request->input('end_date')))
			$this->product->end_date = null;


		if($request->hasFile('pdf')){
			$old_file = $this->product->pdf;
			$this->product->pdf = $this->pdf_location;
		}

		if($request->hasFile('image')){
			$old_image = $this->product->image;
			$this->product->image = $this->image_location;
		}

		$this->product->save();

		@unlink($old_file);
		@unlink($old_image);

		return redirect()->back()->with('success', trans('content.Product_updated!'));
	}

	private function validateProduct(Request $request){

		$rules = [
			'name' => 'required|unique:products',
			'pdf' => 'required|mimes:pdf|max:'.Product::PDF_LIMIT,
			'image' => 'required|mimes:jpeg,jpg,png,bmp|max:'.Product::IMAGE_LIMIT,
			'category_id' => 'required'
		];

		if($this->product !== null){
			$rules['name'] = 'required';
			$rules['image'] = 'mimes:jpeg,jpg,png,bmp|max:'.Product::IMAGE_LIMIT;
			$rules['pdf'] = 'mimes:pdf|max:'.Product::PDF_LIMIT;
		}

		$validator = Validator::make($request->all(), $rules,[
			'category_id.required' => trans('content.The_category_field_is_required.'),
			'name.required' => trans('content.Product_name_is_required'),
			'pdf.required' => trans('content.PDF_field_is_required'),
			'image.required' => trans('content.Image_field_is_required'),
			'image.mimes' => trans('content.Image_not_valid.'),
			'pdf.mimes' => trans('content.Pdf_not_valid'),
			'image.max' => 'Image max limit '.Product::IMAGE_LIMIT.'KB',
			'pdf.max' => 'PDF max limit '.Product::PDF_LIMIT.'KB',
		]);

		$validator->after(function($validator) use ($request)
		{		
			$file = $request->file('pdf');
			if ($request->hasFile('pdf'))
			{
				$extension = $file->getClientOriginalExtension();
				$newFilename = date('YmdHis').'.'.$extension;
				$this->pdf_location = Product::PDF_PATH.$newFilename;
				$file->move(Product::PDF_PATH, $newFilename);
			}

			$image = $request->file('image');
			if ($request->hasFile('image'))
			{
				$extension = $image->getClientOriginalExtension();
				$newFilename = date('YmdHis').'.'.$extension;
				$this->image_location = Product::PDF_PATH.$newFilename;
				$image->move(Product::PDF_PATH, $newFilename);
			}

		});

		return $validator;
	}

	public function postDelete(Request $request){

		$product = Product::find($request->product_id);
		$product->delete();

		return redirect('admin/products')->with('success', $product->name.' '.trans('content.Delete'));

	}

	public function getCsv(){
        Product::create_csv();	
    }

}
