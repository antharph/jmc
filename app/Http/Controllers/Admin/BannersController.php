<?php 
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Banner;
use App\Product;

class BannersController extends Controller {

    private $image_location;

    public function __construct()
	{
		// $this->middleware('auth');
    }
    
    public function getIndex(){
        return view('admin.banners.index');
    }

    public function getEdit(){
        $banner = Banner::first();
        $data['banner'] = $banner;

        return view('admin.banners.edit',$data);
    }

    public function postEdit(Request $request){
		$id = $request->input('id');
		$banner = Banner::find($id);
		if($banner == null)
			abort(404);

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'image' => 'mimes:jpeg,jpg,png',
		]);

		$validator->after(function($validator) use ($request,$banner)
		{
			if ($request->hasFile('image'))
			{
				$file = $request->file('image');
				if($this->validImage($file) === false){
					$validator->errors()->add('image', 'Image not valid');
				}
			}
		});

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator)->withInput();
		}

		$banner->name = $request->input('name');
		$banner->description = $request->input('description');

		if($request->hasFile('image')){
			$old_image = $banner->image;
			$banner->image = $this->image_location;
		}

        $banner->save();
        
		@unlink($old_image);

		return redirect()->back()->with('success', 'Banner updated!');
    }
    
    private function validImage($file){

		$extension = strtolower($file->getClientOriginalExtension());
		if(in_array($extension,Product::VALID_IMAGES)){
			$newFilename = date('YmdHis').'.'.$extension;
			$this->image_location = Product::PDF_PATH.$newFilename;
			$file->move(Product::PDF_PATH, $newFilename);
			return true;
		}

		return false;
	}

}