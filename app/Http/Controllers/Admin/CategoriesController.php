<?php 
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Category;
use App\Product;

class CategoriesController extends Controller {

    const LIMIT = 10;
    const SLUG_SEPARATOR = '-';
    private $slug;
    private $category;

    public function __construct()
	{
        // $this->middleware('auth');
    }
    
    public function getIndex(){
        $data['categories'] = Category::with(['products'])
                            ->orderBy('order','asc')
                            ->paginate(self::LIMIT);
		return view('admin.categories.index',$data);
    }

    public function getAdd(){
        return view('admin.categories.add');
    }

    public function getProducts($category_id){
        $data['products'] = Product::where('category_id',$category_id)
                    ->orderBy('order_in_category','asc')
                    ->paginate(self::LIMIT);

        return view('admin.categories.products',$data);
    }

    public function postAdd(Request $request){

		$validator = $this->validateCategory($request);

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator)->withInput();
        }

        $currentCategories = Category::count();
        $order = $currentCategories+1;
        $lastOrder = Category::orderBy('order','desc')->first();

        if($lastOrder != null && $lastOrder->order > $order){
            $order = $lastOrder->order + 1;
        }

		$category = new Category;
		$category->slug = $this->slug;
        $category->name = $request->input('name');
        $category->order = $order;
		$category->save();
		
		return redirect('admin/categories')->with('success', trans('content.Category_saved!'));
    }

    public function getEdit($id=null){
        $data['category'] = Category::find($id);
        return view('admin.categories.edit',$data);
    }

    public function postEdit(Request $request){

        $this->category = Category::find($request->input('category_id'));

        $validator = $this->validateCategory($request);

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator)->withInput();
        }

        $this->category->slug = $this->slug;
        $this->category->name = $request->input('name');
        $this->category->save();

        return redirect()->back()->with('success', trans('content.Category_updated!'));
    }
    
    private function validateCategory(Request $request){

        if($this->category == null){
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:categories',
            ],
            [
                'name.required' => trans('content.The_category_name_field_is_required.'),
                'name.unique' => trans('content.Category_already_exist!'),
            ]
        );
        }else{
            $validator = Validator::make($request->all(),[
                'name' => 'required',
            ],
            [
                'name.required' => trans('content.The_category_name_field_is_required.')
            ]
        );
        }

		return $validator;

    }

    public function postUpdateorder(Request $request){
        foreach($request->input('order') as $id => $value){
            $category = Category::find($id);
            $category->order = $value;
            $category->save();
        }
    }

    public function postUpdateproductorder(Request $request){
        foreach($request->input('order') as $id => $value){
            $product = Product::find($id);
            $product->order_in_category = $value;
            $product->save();
        }
    }

    public function postDelete(Request $request){

		$category = Category::find($request->category_id);
		$category->delete();

		return redirect('admin/categories')->with('success', $category->name.' '.trans('content.Delete'));

	}
}