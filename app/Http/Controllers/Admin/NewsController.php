<?php 
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\NewsInfo;

class NewsController extends Controller {

    const LIMIT = 20;
    private $newsInfo;
    const SLUG_SEPARATOR = '-';
    private $slug;

	public function __construct()
	{
		// $this->middleware('auth');
    }

    public function getIndex(){
        $data['newsInfo'] = NewsInfo::orderBy('order','asc')->paginate(self::LIMIT);
		return view('admin.news.index',$data);
    }

    public function getAdd(){
        return view('admin.news.add');
    }

    public function postAdd(Request $request){

		$validator = $this->validateNews($request);

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator)->withInput();
        }

        $currentNews = NewsInfo::count();
        $order = $currentNews+1;
        $lastOrder = NewsInfo::orderBy('order','desc')->first();

        if($lastOrder != null && $lastOrder->order > $order){
            $order = $lastOrder->order + 1;
        }

		$this->newsInfo = new NewsInfo;
		$this->newsInfo->slug = $this->slug;
        $this->newsInfo->title = $request->input('title');
        $this->newsInfo->description = $request->input('description');
        $this->newsInfo->privacy = $request->input('privacy');
        $this->newsInfo->url = $request->input('url');
        $this->newsInfo->order = $order;
		$this->newsInfo->save();
		
		return redirect('admin/news')->with('success', trans('content.News/Info_added!'));
    }

    public function getEdit($id=null){
        $data['newsInfo'] = NewsInfo::find($id);
        return view('admin.news.edit',$data);
    }

    public function postEdit(Request $request){

        $this->newsInfo = NewsInfo::find($request->input('news_info_id'));

        $validator = $this->validateNews($request);

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator)->withInput();
        }

        $this->newsInfo->slug = $this->slug;
        $this->newsInfo->title = $request->input('title');
        $this->newsInfo->description = $request->input('description');
        $this->newsInfo->privacy = $request->input('privacy');
        $this->newsInfo->url = $request->input('url');
		$this->newsInfo->save();

        return redirect()->back()->with('success', trans('content.News/Info_updated!'));
    }

    private function validateNews(Request $request){

        $rules = [
            'title' => 'required|unique:news_infos',
        ];

        $messages = [
            'title.required' => trans('content.The_title_field_is_required.'),
            'title.unique' => trans('content.Title_already_exist'),
        ];

        if($this->newsInfo !== null){
            $rules['title'] = 'required';
        }

        $validator = Validator::make($request->all(),$rules,$messages);

		return $validator;

    }

    public function postUpdateorder(Request $request){
        // dd($request->input());
        foreach($request->input('order') as $id => $value){
            $newsInfo = NewsInfo::find($id);
            $newsInfo->order = $value;
            $newsInfo->save();
        }
    }

    public function postDelete(Request $request){

		$newsInfo = NewsInfo::find($request->news_id);
		$newsInfo->delete();

		return redirect('admin/news')->with('success', $newsInfo->title.' '.trans('content.Delete'));

	}
    
}