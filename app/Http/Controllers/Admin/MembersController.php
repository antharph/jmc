<?php 
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\NewsInfo;
use Auth;
use App\MembershipType;

class MembersController extends Controller {

    public function __construct()
	{
		$this->middleware('auth');
    }

    public function getIndex(){
        return view('admin.members.index',['user' => Auth::user()]);
    }

}