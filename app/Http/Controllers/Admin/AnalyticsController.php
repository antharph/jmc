<?php 
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Product;
use App\Analytic;
use DB;

class AnalyticsController extends Controller {

    public function __construct()
	{
		// $this->middleware('auth');
    }

    public function getIndex($report='month'){

        // DB::connection()->enableQueryLog();

        $products = Product::select(
            'categories.name as category_name',
            'products.name',
            DB::raw("(select count(id) from analytics where CURDATE() = DATE(analytics.created_at) and analytics.product_id = products.id and analytics.type = 'dt') as details_today"),
            DB::raw("(select count(id) from analytics where CURDATE() = DATE(analytics.created_at) and analytics.product_id = products.id and analytics.type = 'dl') as download_today"),
            DB::raw("(select count(id) from analytics where YEARWEEK(analytics.created_at, 1) = YEARWEEK(CURDATE(), 1) and analytics.product_id = products.id and analytics.type = 'dt') as details_this_week"),
            DB::raw("(select count(id) from analytics where YEARWEEK(analytics.created_at, 1) = YEARWEEK(CURDATE(), 1) and analytics.product_id = products.id and analytics.type = 'dl') as download_this_week"),
            DB::raw("(select count(id) from analytics where MONTH(analytics.created_at) = MONTH(CURDATE()) and analytics.product_id = products.id and analytics.type = 'dt') as details_this_month"),
            DB::raw("(select count(id) from analytics where MONTH(analytics.created_at) = MONTH(CURDATE()) and analytics.product_id = products.id and analytics.type = 'dl') as download_this_month")
        )
        ->leftJoin('categories','categories.id','=','products.category_id')
        ->get();
        $queries = DB::getQueryLog();
        // dd($queries);

        $data['products'] = $products;
        
        return view('admin.analytics.index',$data);
    }

}