<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Banner;
use App\NewsInfo;
use App\Category;
use Auth;

class CategoriesController extends Controller {

    const LIMIT = 10;

	public function __construct()
	{
		// $this->middleware('auth');
    }

    public function getDetails($category_id){

        $category = Category::with(['products' => function($query)
        {
            $query = Product::publicCondition($query);
            $query->orderBy('order_in_category','asc');
            
        }])->find($category_id);
        $data['category'] = $category;
        return view('categories.details',$data);

    }

    public function getLatest($category_id){
        
        //clicking category will display only latest item in category, not all
        //however there's a red button to display all

        $category = Category::with(['products' => function($query)
        {
            $query = Product::publicCondition($query);
            $query->orderBy('created_at','desc');
            
        }])->find($category_id);

        if(count($category->products)>0){
            $latestProduct = $category->products[0];
            return redirect(url('products/details/'.$latestProduct->id));
        }else{
            return redirect(url('categories/details/'.$category_id));
        }

    }

}