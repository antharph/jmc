<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('auth/login', function(){
	return redirect('login');
});

Route::get('auth/register', function(){
	abort(404);
});

Route::get('password/email', function(){
	abort(404);
});

Route::get('/', 'ProductsController@getIndex');

Route::get('home', 'HomeController@index');

Route::group(['prefix' => 'admin','middleware' => 'admin'], function()
{
	// Route::get('products', 'Admin\ProductsController@index');
	Route::controllers(
		[
			'products' => 'Admin\ProductsController',
			'banners' => 'Admin\BannersController',
			'categories' => 'Admin\CategoriesController',
			'news' => 'Admin\NewsController',
			'analytics' => 'Admin\AnalyticsController'
		]
	);
});

Route::controllers(
	[
		'products' => 'ProductsController',
		'categories' => 'CategoriesController',
		'login' => 'LoginController',
		'members' => 'Admin\MembersController'
	]
);


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
