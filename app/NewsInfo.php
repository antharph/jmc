<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsInfo extends Model {
    use SoftDeletes;
    protected $table = 'news_infos';

    protected $dates = ['deleted_at'];
    protected $guarded = [];
}