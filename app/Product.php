<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Category;
use Excel;
use Auth;

class Product extends Model {

	use SoftDeletes;

	const PDF_LIMIT = '300000'; //300MB
	const PDF_PATH = 'public/products/';
	const VALID_IMAGES = ['png','jpeg','jpg'];
	const IMAGE_LIMIT = '5000'; //5MB

	protected $dates = ['deleted_at'];
	protected $guarded = [];

	public static function privacy(){
		return [
			trans('content.Public'),
			trans('content.Member_Only'),
			trans('content.Private'),
		];
	}

	public function category()
    {
        return $this->belongsTo('App\Category');
	}

	public function analytics()
    {
        return $this->hasMany('App\Analytic');
    }

	public static function create_csv(){

		$filename = 'products.csv';
        @unlink($filename);
		// exec('touch '.$filename);

		$products = Product::select(
			'products.id',
			'products.name',
			'products.title1',
			'products.title2',
			'products.description',
			'products.price',
			'products.privacy',
			'products.home_display',
			'products.pdf',
			'products.image',
			'categories.name as category_name'
			)
		->leftJoin('categories','categories.id','=','products.category_id')
		->get()->toArray();

		$columns = array(
			'ID',
			trans('content.Product'),
			trans('content.Title_1'),
			trans('content.Title_2'),
			trans('content.Description'),
			trans('content.Price'),
			trans('content.Privacy'),
			trans('content.Display_on_top_page'),
			'PDF',
			trans('content.Image'),
			trans('content.Category'),
		);
		
		array_unshift($products , $columns);

		Excel::create('Product', function($csv) use($products) {
            $csv->sheet('sheet1', function($sheet) use ($products) {
                $sheet->fromArray($products, null, 'A1', false, false);
                $sheet->setFontFamily('MS P Gothic');
            });
		}, 'UTF-8')->download('xls'); 
		//UTF-8
	}
	
	public static function publicCondition($query){

		if (!Auth::check())
			$query->where('privacy', '=',0);

		//2 is private
		return $query->where('privacy', '!=',2)
		->where(function($q){
			$q->whereRaw('release_date is null')
			->orWhereRaw('release_date <= NOW()');
		})
		->where(function($q){
			$q->whereRaw('end_date is null')
			->orWhereRaw('DATE_FORMAT(end_date,"%Y-%m-%d 23:59:59") >= NOW()');
		});
	}


}
