<?php namespace App;

class MembershipType{
    const MEMBERSHIPS = [
		"08" => 'Premium_Member',
		"09" => 'General_Member',
		"10" => 'General_Member_Senior',
		"11" => 'Student_Membership',
		"12" => 'Junior_Member',
		"13" => 'Premium_Member_Senior',
	];
}